# 日本主要エリア.JSON
日本の主要地域を独自エリアで区切ってJSONファイルにしました。  
activateはリストの開閉判断に使っていました。適宜使いやすいように変更してください。

```
{
  "label": "北海道",
  "slug": "hokkaido",
  "activate": false,
  "area": [
    {"value": "札幌周辺", "url": ""},
    {"value": "旭川周辺", "url": ""},
    {"value": "道東", "url": ""},
    {"value": "道南", "url": ""},
    {"value": "道北", "url": ""}
  ]
},
```
